# 24h calendar watchface for Watchmaker

Deze watchface moet gebruikt worden met de [Watchmaker app voor Android](https://play.google.com/store/apps/details?id=slide.watchFrenzy).

Je hem ook direct van [Facerepo.com](https://facerepo.com/app/faces/details/24h-calendar-watchface-15099855321) importeren aan de hand van onderstaande QR-code.

![](http://gitlab.com/mdsnouck/Moto360watchface/raw/master/preview.png)
![](http://gitlab.com/mdsnouck/Moto360watchface/raw/master/QR.png)
